'use strict';

var gulp = require('gulp'),
  fileinclude = require('gulp-file-include'),
  jshint = require('gulp-jshint'),
  stylish = require('jshint-stylish'),
  sass = require('gulp-sass'),
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer-core'),
  browserify = require('browserify'),
  minifycss = require('gulp-minify-css'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  express = require('express'),
  app = express(),
  tinylr;

gulp.task('express', function() {
  app.use(require('connect-livereload')({port: 4002}));
  __dirname =  __dirname + "/dist";
  app.use(express.static(__dirname));
  app.listen(4000);
});

gulp.task('livereload', function() {
  tinylr = require('tiny-lr')();
  tinylr.listen(4002);
});

function notifyLiveReload(event) {
  var fileName = require('path').relative(__dirname, event.path);

  tinylr.changed({
    body: {
      files: [fileName]
    }
  });
}

gulp.task('lint', function() {
    return gulp.src(['js/libs/*js', 'js/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

gulp.task('image', function() {
    return gulp.src('img/**',{base: '.'})
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('fonts', function() {
    return gulp.src('fonts/**')
        .pipe(gulp.dest('dist/fonts'));
});

gulp.task('html', function() {
    return gulp.src(['html/*.html'])
    .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('styles', function() {
    var processors = [
        autoprefixer({browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1']})
    ];
    return gulp.src('css/*.scss')
        .pipe(sass())
        .pipe(postcss(processors))
        .pipe(gulp.dest('dist/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('dist/css'));
});

gulp.task('scripts', function() {
    return gulp.src(['js/libs/jquery-2.1.4.min.js', 'js/libs/flickity.pkgd.min.js', 'js/*.js'])
        .pipe(concat('js/main.min.js'))
        .pipe(gulp.dest('dist'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
    gulp.watch('js/*.js', ['lint', 'scripts']);
    gulp.watch('css/*.scss', ['styles']);
    gulp.watch('html/*.html', ['html']);
    gulp.watch('img/*.jpg', ['image']);
    gulp.watch('img/*.png', ['image']);
    gulp.watch('img/*.svg', ['image']);
    gulp.watch('dist/*.html', notifyLiveReload);
    gulp.watch('dist/css/*.css', notifyLiveReload);
    gulp.watch('dist/js/*.js', notifyLiveReload);
});

gulp.task('default', ['express', 'lint', 'image', 'fonts', 'styles', 'html', 'scripts', 'livereload', 'watch']);

