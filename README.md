## Cloning

```git remote rm origin
git remote add origin git@path-to-your-own-repo.git
git push origin master```

## Installation

```npm install
gulp```

If you run into any issues with npm versioning, you can update the package.json by running:

```npm install -g npm-check-updates
npm-check-updates -u
npm install```

Step 1 only necessary if `npm-check-updates` hasn't been installed globally