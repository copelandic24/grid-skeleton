'use strict';

$('html').click(function() {
	$('.top-nav').removeClass('active');
});

$('.mob-nav').on('click', function() {
	event.stopPropagation();
	$('.top-nav').toggleClass('active');
});

$('.main-gallery').flickity({
  cellAlign: 'left',
  contain: true,
  wrapAround: true,
  imagesLoaded: true
});